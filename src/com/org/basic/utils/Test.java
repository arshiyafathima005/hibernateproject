package com.org.basic.utils;
import com.org.basic.dao.StudentDAO;
import com.org.basic.dao.StudentHqlDAO;
import com.org.basic.dto.Student;

public class Test {

	public static void main(String[] args) {
		Student student = new Student();
		//student.setId(102L);
	student.setName("ziya");
	student.setBranch("ca");
	student.setAge(27L);
	student.setContactNumber(98004567309L);
	
    StudentDAO studentDAO = new StudentDAO();
    studentDAO.saveStudentDetails(student) ;
    System.out.println("operation succssful");
	//studentDAO.updateStudentById(100L, 5675936L);*/
	
	/*Student std = studentDAO.getStudentById(200L);
	System.out.println(std);*/
	
	//studentDAO.saveStudentDetails(student);
	
	StudentHqlDAO studentHqlDAO = new StudentHqlDAO();
	/*hql retrive
	studentHqlDAO.getStudents().forEach(a->{
		System.out.println(a);
	});
	*/
	//hql retrive
	/*Student std = studentHqlDAO.getStudentByContactNumber(4567399L);	
	System.out.println(std);*/

	studentHqlDAO.updateContactNumberById(1L,8496875561L);//hql update
	studentHqlDAO.deleteById(1L);//hql delete
	}

}
