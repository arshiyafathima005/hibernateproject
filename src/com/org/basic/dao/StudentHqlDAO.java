package com.org.basic.dao;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
 import com.org.basic.dto.Student;

public class StudentHqlDAO {
	public List<Student> getStudents() {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String hql="from Student";
		Query query = session.createQuery(hql);
		List<Student> list = query.list();
		return list;
	}
	
	public Student getStudentByContactNumber(Long contactNumber) {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String hql="from Student where contactNumber=:contact";
		Query query = session.createQuery(hql);
		query.setParameter("contact", contactNumber);
		Student student = (Student) query.uniqueResult();
		return student;
	}
	public void updateContactNumberById(Long id,Long contactNumber)
	{
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String hql="update Student set contactNumber=:contact where id=:id";
		Query query = session.createQuery(hql);
		query.setParameter("contact",contactNumber);
		query.setParameter("id",id);
		int rowCount=query.executeUpdate();
		if(rowCount!=0)
		{
			System.out.println("Data updated Succsessfully");
		}else
		{
			System.out.println("Update operation failed");
		}
		
		
	}
	public void deleteById(Long id)
	{Configuration configuration = new Configuration();
	configuration.configure();
	SessionFactory sessionFactory = configuration.buildSessionFactory();
	Session session = sessionFactory.openSession();
	String hql="delete from Student where id=:id";
	Query query = session.createQuery(hql);
	query.setParameter("id",id);
	int rowCount=query.executeUpdate();
	if(rowCount!=0)
	{
		System.out.println("Data deleted Succsessfully");
	}else
	{
		System.out.println("delete operation failed");
	}
	}
}
